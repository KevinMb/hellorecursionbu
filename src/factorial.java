public class Factorial{
	public int factorial(int x){
		//loop invariant. Is something that is always constant
		
		int factorial = 1;
		int counter = 1;
		
		while(counter < x){
			factorial = factorial * counter;
			counter++;
		}
		//counter is going to be equal to x
		// factorial ? for the loop invariant to be preserved.
		//there was a bug, because 
		return factorial;
	
	}
	public static int fac (int x) {
		//what happens when x = 0?
		if (x == 0 || x == 1){
			return 1
		}
		return x*fac(x-1);
	}
	
	public static void main(String[] args){
		System.out.println(Factorial.fac())
	}
}